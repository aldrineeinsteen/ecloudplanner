package com.gnalam.ecloudplanner.auth.config.dev;

import com.gnalam.ecloudplanner.auth.model.ContactDetail;
import com.gnalam.ecloudplanner.auth.model.ContactType;
import com.gnalam.ecloudplanner.auth.model.Person;
import com.gnalam.ecloudplanner.auth.model.User;
import com.gnalam.ecloudplanner.auth.repository.ContactDetailRepository;
import com.gnalam.ecloudplanner.auth.repository.PersonRepository;
import com.gnalam.ecloudplanner.auth.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Arrays;
import java.util.List;

@Configuration
@Profile("dev")
@AllArgsConstructor
public class DevDataSetUpCLR implements CommandLineRunner {

    private UserRepository userRepository;

    private ContactDetailRepository contactDetailRepository;

    private PersonRepository personRepository;

    @Override
    public void run(String... args) throws Exception {
        User admin = new User();
        admin.setUsername("admin");
        admin.setPassword("Admin@123");

        admin = userRepository.save(admin);

        Person adminPerson = new Person();
        adminPerson.setUser(admin);
        adminPerson.setFirstName("Administrator");
        adminPerson.setLastName("Profile");

        adminPerson = personRepository.save(adminPerson);

        ContactDetail adminContact = new ContactDetail();
        adminContact.setContact("admin@example.com");
        adminContact.setType(ContactType.EMAIL);
        adminContact.setPerson(adminPerson);

        contactDetailRepository.save(adminContact);

        //batch test
        User u1 = new User("u1", "p1");
        User u2 = new User("u2", "p2");
        User u3 = new User("u3", "p3");
        User u4 = new User("u4", "p4");
        User u5 = new User("u5", "p5");
        List<User> customers = Arrays.asList(u1, u2, u3, u4, u5);
        userRepository.saveAll(customers);
    }
}
