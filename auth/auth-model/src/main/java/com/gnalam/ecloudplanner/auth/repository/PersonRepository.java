package com.gnalam.ecloudplanner.auth.repository;

import com.gnalam.ecloudplanner.auth.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PersonRepository extends JpaRepository<Person, UUID> {

}
