package com.gnalam.ecloudplanner.auth.model;

import com.gnalam.ecloudplanner.common.base.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Setter
@Getter
public class ContactDetail extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "person_uuid", nullable = false)
    private Person person;

    private ContactType type;

    private String contact;
}
