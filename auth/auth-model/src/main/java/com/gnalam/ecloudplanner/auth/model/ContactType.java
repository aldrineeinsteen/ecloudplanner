package com.gnalam.ecloudplanner.auth.model;

public enum ContactType {
    MOBILE, EMAIL
}
