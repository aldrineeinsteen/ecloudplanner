package com.gnalam.ecloudplanner.auth.repository;

import com.gnalam.ecloudplanner.auth.model.ContactDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ContactDetailRepository extends JpaRepository<ContactDetail, UUID> {
}
