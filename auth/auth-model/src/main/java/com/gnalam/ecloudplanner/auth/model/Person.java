package com.gnalam.ecloudplanner.auth.model;

import com.fasterxml.jackson.annotation.JsonRawValue;
import com.gnalam.ecloudplanner.common.base.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotEmpty;

@Entity
@Getter
@Setter
public class Person extends BaseEntity {

    @JsonRawValue
    @OneToOne
    private User user;

    @NotEmpty
    private String firstName;

    private String middleName;

    @NotEmpty
    private String lastName;
}
