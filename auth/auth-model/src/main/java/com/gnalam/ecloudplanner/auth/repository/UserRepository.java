package com.gnalam.ecloudplanner.auth.repository;

import com.gnalam.ecloudplanner.auth.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

    Optional<User> findUserByUsername(String username);

    Boolean existsUserByUsername(String username);
}
