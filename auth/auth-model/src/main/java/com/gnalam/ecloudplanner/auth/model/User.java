package com.gnalam.ecloudplanner.auth.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gnalam.ecloudplanner.common.base.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;

@Entity
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class User extends BaseEntity {

    @NotEmpty
    private String username;

    @JsonIgnore
    @NotEmpty
    private String password;
}
